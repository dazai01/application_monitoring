FROM python
COPY . /app
WORKDIR /app
RUN pip install -r monitor/requirements.txt
EXPOSE 5000
ENTRYPOINT ["flask", "run"]