from monitor import db


class User(db.Document):
    mongo_id = db.ObjectIdField(required=True, db_field='_id')
    name = db.StringField()
    password = db.StringField()


class Car(db.Document):
    mongo_id = db.ObjectIdField(required=True, db_field='_id')
    mat = db.StringField()
    token = db.StringField()


class Log(db.Document):
    mongo_id = db.ObjectIdField(required=True, db_field='_id')
    time_stamp = db.StringField()
    message = db.StringField()
    car_mat = db.StringField()

class Position(db.Document):
    mongo_id = db.ObjectIdField(required=True, db_field='_id')
    car_mat = db.StringField()
    time_stamp = db.StringField()
    lattitude = db.FloatField()
    longitude = db.FloatField()
