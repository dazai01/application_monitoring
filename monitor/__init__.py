from flask import Flask
from flask_cors import CORS
from firebase_admin import credentials, initialize_app
from flask_mongoalchemy import MongoAlchemy

connection_string = "mysql+pymysql://root:pwd@172.17.0.1:3306/monitor"
upload_folder = 'binaries'
ALLOWED_EXTENSIONS = {'bin', 'sh', 'gz'}
ALLOWED_MIME_TYPES = {'application/x-sh', 'application/octet-stream', 'application/gzip'}

app = Flask(__name__)
cors = CORS(app)
app.config['MONGOALCHEMY_DATABASE'] = "monitor"
app.config['SECRET_KEY'] = 'a38115391bdf5aff0f2bf0ee819b20373f1985b6aa3123f067216e9977a640add076e7dfae8cdc91b23fe6868860b1b2966b'
app.config['UPLOAD_DIR'] = upload_folder
db = MongoAlchemy(app)

app.config['REAL_TIME_DATABASE_URI'] = "https://akkurad-1629364845909-default-rtdb.europe-west1.firebasedatabase.app/update/"
app.config['MOTOR_SLAVE2'] = 201
app.config['MOTOR_SLAVE1'] = 200
app.config["DASHBOARD"] = 100
app.config['GATEWAY'] = 160
app.config['SETUP'] = 1
app.config['BMS'] = 180
cred = credentials.Certificate("akkurad-1629364845909-firebase-adminsdk-hn1qw-dbbb7cc478.json")
initialize_app(cred, {'storageBucket': 'akkurad-1629364845909.appspot.com'})

from monitor.controllers import auth_controller, car_controller, file_controller
