from monitor import app
from flask import request, json
import jwt, logging
from functools import wraps
from monitor.entities.models import User
from datetime import datetime
from werkzeug.exceptions import HTTPException, Unauthorized


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'Authorization' in request.headers:
            token = request.headers.get('Authorization')

        if not token:
            raise Unauthorized('Could not authenticate')

        try:
            data = jwt.decode(token[7:], app.config.get('SECRET_KEY'))
            current_user = User.query.filter_by(name=data.get('name')).first()
        except:
            raise Unauthorized('invalid jwt')

        return f(current_user, *args, **kwargs)

    return decorated

def get_logger_config(name):
    logging.basicConfig(filename="monitor.log", filemode='a', format="%(asctime)s  %(levelname)s  %(name)s :     %(message)s")
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    if name is not None:
        logger.name = name
    return logger

def log_headers(name):
    def decorate(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            headers = request.headers
            logger = get_logger_config(name)
            for header in headers:
                logger.info(header)
                logger.info("---------------------------------------------------------------------------------")
            return f(*args, **kwargs)
        return wrapper
    return decorate

def log_request_body(name):
    def decorate(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            body = request.get_json()
            logger = get_logger_config(name)
            logger.info(body)
            logger.info("---------------------------------------------------------------------------------")
            return f(*args, **kwargs)
        return wrapper
    return decorate

@app.errorhandler(HTTPException)
def handle_exception(e):
    error_response = {'time_stamp': datetime.utcnow().__str__(), 'error': e.name, 'message': e.description,
                      'path': request.path, 'status': e.code}
    return app.response_class(response=json.dumps(error_response), status=e.code, mimetype='application/json')
