from datetime import datetime, timedelta

from werkzeug.exceptions import BadRequest
from monitor import db, app
from flask import json, request
from flask_cors import cross_origin
from bson.objectid import ObjectId
from monitor.entities.models import User
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from monitor.config.config import token_required


@app.route('/', methods=['GET'])
@token_required
@cross_origin()
def find_all(current_user):
    users = User.query.all()
    response = []
    for user in users:
        output = {'id': user.mongo_id.__str__(), 'name': user.name, 'password': user.password}
        response.append(output)

    return app.response_class(response=json.dumps(response), status=200, mimetype='application/json')


@app.route('/', methods=['POST'])
@cross_origin()
def signup():
    authentication_request = request.get_json()
    password = generate_password_hash(
        authentication_request.get('password'), method='sha256')
    new_user = User(mongo_id=ObjectId(), name=authentication_request.get('name'), password=password)
    db.session.add(new_user)
    return app.response_class(response=json.dumps({'message': 'user created successfully', 'status': True}), status=200,
                              mimetype='application/json')


@app.route('/login', methods=['POST'])
@cross_origin()
def login():
    login_request = request.get_json()
    if not login_request.get('username') or not login_request.get('password'):
        raise BadRequest('required name and password')

    user = User.query.filter_by(name=login_request.get('username')).first()

    if not user:
        raise BadRequest('user not found with name ' +
                         str(login_request.get('username')))

    if check_password_hash(user.password, login_request['password']):
        token = jwt.encode({'name': user.name, 'exp': datetime.utcnow(
        ) + timedelta(days=5)}, app.config.get('SECRET_KEY'))
        user_dict = {'id': user.mongo_id.__str__(), 'name': user.name}
        jwt_token = {'type': 'Bearer', 'token': token.decode('UTF-8')}
        response = {'user': user_dict, 'token': jwt_token}
        return app.response_class(response=json.dumps(response), status=200, mimetype='application/json')

    raise BadRequest('Could not authenticate')
