from monitor.services.services import upload_file_to_firebase, allowed_files
import os
from werkzeug.exceptions import BadRequest
from werkzeug.utils import secure_filename
from monitor import app
from monitor.config.config import log_headers, log_request_body, token_required
from flask import json, request
from flask_cors import cross_origin


@app.route('/upload', methods=['POST'])
#@token_required
@log_headers(__name__)
@log_request_body(__name__)
@cross_origin()
def upload_file():
    if 'file' not in request.files:
        raise BadRequest('No file part')
    file = request.files.get('file')
    file_path = ''
    if file.filename == '':
        raise BadRequest('No file selected')
    if file and allowed_files(file):
        file_name = secure_filename(file.filename)
        file_path = os.path.join(app.config.get('UPLOAD_DIR'), file_name)
        file.save(file_path)
        upload_file_to_firebase(file_path)
        return app.response_class(response=json.dumps({'message': 'file uploaded', 'status': True}), status=201, content_type='application/json')
    raise BadRequest('Not allowed file mime type')
