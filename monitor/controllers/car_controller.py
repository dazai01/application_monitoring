from datetime import datetime
from random import randint, random
import secrets
from monitor.entities.models import Car, Log, Position
from monitor import app, db
from monitor.config.config import token_required
from flask import request, json
from flask_cors import cross_origin
from bson import ObjectId


@app.route('/cars', methods=['POST'])
#@token_required
@cross_origin()
def add_car(current_user):
    data = request.get_json()
    car = Car(mongo_id=ObjectId(), mat=data.get(
        'mat'), token=secrets.token_bytes(16).hex())
    db.session.add(car)
    return app.response_class(response=json.dumps({'message': 'Car added successfully', 'status': True}), status=200,
                              mimetype='application/json')

@app.route('/cars', methods=['GET'])
@cross_origin()
def get_all_cars():
    response = []
    images = ['https://firebasestorage.googleapis.com/v0/b/rent-your-bike-297409.appspot.com/o/NEW_FOLDER%2F1_2021_bugatti_chiron_super_sport.jpg?alt=media', 
    'https://firebasestorage.googleapis.com/v0/b/rent-your-bike-297409.appspot.com/o/NEW_FOLDER%2F2022-chevrolet-corvette-z06-1607016574.jpg?alt=media', 
    'https://firebasestorage.googleapis.com/v0/b/rent-your-bike-297409.appspot.com/o/NEW_FOLDER%2Fhttps _specials-images.forbesimg.com_imageserve_5d35eacaf1176b0008974b54_2020-Chevrolet-Corvette-Stingray_960x0.jpg%3FcropX1%3D790%26cropX2%3D5350%26cropY1%3D784%26cropY2%3D3349.jpeg?alt=media', 
    'https://firebasestorage.googleapis.com/v0/b/rent-your-bike-297409.appspot.com/o/NEW_FOLDER%2Findex.jpeg?alt=media']
    for i in range(20):
        car = {'id': ObjectId().__str__(), 'amount': random()*100.0, 'date': datetime.utcnow().__str__(), 'category': 'Galopp', 'image_url': images[randint(0, 3)]}
        response.append(car)
    return app.response_class(response=json.dumps(response), status=200, mimetype='application/json')

@app.route('/cars/logs/add', methods=['POST'])
@cross_origin()
def add_log():
    data = request.get_json()
    car = Car.query.filter_by(token=data.get('token')).first()
    if car is None:
        response = {
            'message': 'No cars were found with this token', 'status': False}
        return app.response_class(response=json.dumps(response), status=404, mimetype='application/json')
    log = Log(mongo_id=ObjectId(), time_stamp=datetime.utcnow().__str__(), message=data.get('message'),
              car_mat=car.mat)
    db.session.add(log)
    return app.response_class(response=json.dumps({'message': 'Data added', 'status': True}), status=200,
                              mimetype='application/json')


@app.route('/cars/logs/all', methods=['GET'])
@token_required
@cross_origin()
def get_all_logs(current_user):
    logs = Log.query.all()
    response = []
    for log in logs:
        output = {'id': log.mongo_id.__str__(), 'time_stamp': log.time_stamp,
                  'message': log.message, 'car_mat': log.car_mat}
        response.append(output)
    return app.response_class(response=json.dumps(response), status=200, mimetype='application/json')


@app.route('/cars/position/push', methods=['POST'])
@cross_origin()
def add_car_position():
    data = request.get_json()
    car = Car.query.filter_by(token=data.get('token')).first()
    if car is None:
        response = {
            'message': 'No cars were found with this token', 'status': False}
        return app.response_class(response=json.dumps(response), status=404, mimetype='application/json')
    position = Position(mongo_id=ObjectId, time_stamp=datetime.utcnow().__str__(), 
                        car_mat=car.mat, latitude=data.get('lat'), longitude=data.get('long'))
    db.session.add(position)
    return app.response_class(response=json.dumps({'message': 'Data added', 'status': True}), status=200,
                              mimetype='application/json')
