import hashlib
import os
import requests
from firebase_admin import storage
from monitor import ALLOWED_EXTENSIONS, app
from monitor import ALLOWED_MIME_TYPES

def upload_file_to_firebase(file_path) -> None:

    #Put your local file path 
    #file_path = "binaries/gateway_test_F4.bin"
    bucket = storage.bucket()
    blob = bucket.blob(file_path)
    blob.upload_from_filename(file_path)

    #Opt : if you want to make public access from the URL
    blob.make_public()

    if file_path.endswith('sh'):
        file_name = file_path[file_path.rindex('/')+1:-3]
    elif file_path.endswith('gz'):
        file_name = file_path[file_path.rindex('/')+1:-7]

    else:
        file_name = file_path[file_path.rindex('/')+1:-4]
    size = os.path.getsize(file_path)
    link = blob.public_url
    sha256 = sha_256_hash(file_path)
    id = app.config.get(file_name.upper())
    data = {'size': size, 'link': link, 'sha256': sha256, 'id': id}
    requests.api.put(app.config.get('REAL_TIME_DATABASE_URI')+file_name+".json", json=data)


def sha_256_hash(file_path) -> str:
    hash = ''
    with open(file_path, 'rb') as file:
        f_bytes = file.read()
        hash = hashlib.sha256(f_bytes).hexdigest()
        file.close()
    return hash


def allowed_files(file) -> bool:
    return '.' in file.filename and file.filename.split('.')[-1].lower() in ALLOWED_EXTENSIONS and file.mimetype in ALLOWED_MIME_TYPES